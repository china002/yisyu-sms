<?php

namespace yisyus\utils;

abstract class Enum
{
    /*
     * URL地址
     */
    const SMS_URL = 'http://sms.yisyu.com';

    /*
     * 物流查询URI
     */
    const EXPRESS_QUERY_URI = '/api/express/query';

    /*
     * 短信发送URI
     */
    const SMS_URI = '/api/yy_sms/send';


    /*
     * 成功
     */
    const SUCCESS = 200;

}