<?php

namespace yisyus;

use yisyus\utils\AES;

abstract class YyBase
{
    /*
     * 邑予APPID
     */
    private $appId;

    /*
     * 邑予APP秘钥
     */
    private $appSecret;


    /**
     * 初始化参数
     * @param string $appId
     * @param string $appSecret
     * @return mixed
     */
    abstract static function init(string $appId,string $appSecret);
    /**
     * 获取邑予APPID
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * 设置邑予APPID
     * @param $appId
     * @return void
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
    }

    /**
     * 获取邑予短信APP秘钥
     * @return string
     */
    public function getAppSecret(): string
    {
        return $this->appSecret;
    }

    /**
     * 设置邑予APP秘钥
     * @param $appSecret
     * @return void
     */
    public function setAppSecret($appSecret)
    {
        $this->appSecret = $appSecret;
    }


    /**
     * 加密参数
     * @param string $string
     * @return array
     */
    protected function setParam(array $data): array
    {
        $string = json_encode($data);
        $string = AES::encrypt($string, $this->getAppId(), $this->getAppSecret());
        return [
            'app_id' => $this->getAppId(),
            'encrypt_str' => $string
        ];
    }
}