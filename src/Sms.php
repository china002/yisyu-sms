<?php

namespace yisyus;

use yisyus\exception\YisyusSmsException;
use yisyus\utils\AES;
use yisyus\utils\Client;
use yisyus\utils\Enum;

class Sms extends YyBase
{


    /**
     * 初始化参数
     * @param string $appId  邑予APPID
     * @param string $appSecret 邑予APP秘钥
     * @return Sms
     */
    public static function init(string $appId, string $appSecret): Sms
    {
        $sms = new Sms();
        $sms->setAppId($appId);
        $sms->setAppSecret($appSecret);
        return $sms;
    }

    /**
     * 单发短信
     * @param $mobile string 手机号
     * @param $sms_content string 短信内容
     * @param $sms_temp_id string 短信模板ID
     * @return bool
     */
    public function sendVerifyCode(string $mobile, string $sms_content, string $sms_temp_id): bool
    {
        $data = [
            'mobiles' => $mobile,
            'temp_id' => $sms_temp_id,
            'content' => $sms_content,
            'batch' => 0,
        ];
        $content = Client::getClient(Enum::SMS_URL)
            ->post(Enum::SMS_URI, $this->setParam($data))
            ->body()->content(true);
        if (!$content) return false;
        if ($content['status'] == Enum::SUCCESS) return true;
        throw new  YisyusSmsException($content['msg'], $content['status']);
    }

    /**
     * 批量发短信
     * @param array $mobiles 手机号
     * @param string $sms_content 短信内容
     * @param string $sms_temp_id 短信模板ID
     * @return bool
     */
    public function batchSendVerifyCode(array $mobiles, string $sms_content, string $sms_temp_id): bool
    {
        $data = [
            'mobiles' => implode(',', $mobiles),
            'temp_id' => $sms_temp_id,
            'content' => $sms_content,
            'batch' => 1,
        ];
        $content = Client::getClient(Enum::SMS_URL)
            ->post(Enum::SMS_URI, $this->setParam($data))
            ->body()->content(true);
        if (!$content) return false;
        if ($content['status'] == Enum::SUCCESS) return true;
        throw new  YisyusSmsException($content['msg'], $content['status']);
    }
}