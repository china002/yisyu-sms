<?php

namespace yisyus;

use yisyus\exception\YisyusSmsException;
use yisyus\utils\Client;
use yisyus\utils\Enum;

class Express extends YyBase
{

    /**
     * @param string $appId
     * @param string $appSecret
     * @return Express
     */
    static function init(string $appId, string $appSecret): Express
    {
        $express = new Express();
        $express->setAppId($appId);
        $express->setAppSecret($appSecret);
        return $express;
    }

    /**
     * 查询物流信息
     * @param string $no
     * @param string|null $type
     * @return array|bool
     */
    public function queryExpress(string $no, string $type = null)
    {
        $data = ['no' => $no];
        if ($type)$data['type'] = $type;
        $content = Client::getClient(Enum::SMS_URL)
            ->get(Enum::EXPRESS_QUERY_URI, $this->setParam($data))
            ->body()->content(true);
        if (!$content) return false;
        if ($content['status'] == Enum::SUCCESS) return $content;
        throw new  YisyusSmsException($content['msg'], $content['status']);
    }
}