# 邑予短信

适用于开通邑予短信功能后使用。

## 安装

~~~
composer require yisyus/yisyu-sms
~~~

## 短信发送示例：

~~~php
use yisyus\Sms;


//初始化配置
$object = Sms::init('分发的应用APP_ID', '分发的APP_SECRET');              
// 单发验证码
$object->sendVerifyCode(133xxxx5233,xxxxxx,xxx);//手机号，短信内容，短信模板

//群发手机号
$batchMobile = array(
    13123456789,
    15123456789,
    16123456789,
);
//群发验证码
$object->batchSendVerifyCode($batchMobile,xxxxxx,xxx);//手机号，短信内容，短信模板
    
~~~

## 物流查询示例：

~~~php
use yisyus\Express;


//初始化配置
$object = Express::init('分发的应用APP_ID', '分发的APP_SECRET');    
          
// 查询物流
$object->->queryExpress('物流单号','[可选（快递公司字母简写）]type');

~~~
